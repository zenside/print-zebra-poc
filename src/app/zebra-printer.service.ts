import {Injectable} from '@angular/core';
import {Platform} from '@ionic/angular';
import {BLE} from '@ionic-native/ble/ngx';

const SCAN_DELAY = 10;

export enum PRINT_ERROR_CODES {
        NO_PRINTER_FOUND,
        PRINT_ERROR
}

const ZEBRA_SERVICE = '38eb4a80-c570-11e3-9507-0002a5d5c51b';
const PRINTER_NAME_PREFIX = 'ZBP';

/**
 * To print label the Zebra printer should be turned on and the bluetooth of the device should be activated
 * (the POC doesn't explicitly check bluetooth status before scan).
 */
@Injectable({
        providedIn: 'root'
})
export class ZebraPrinterService {

        constructor(public plt: Platform, private ble: BLE) {
        }

        /**
         * try to print some zpl code to a Zebra printer accesible by bluetooth.
         *
         * First search for a bluetooth device where name starts with "ZPB"
         * If not found, search for a device with ZEBRA service configured (slower)
         *
         * @param zblMessage zbl code to print
         */
        async printZbl(zblMessage: string) {
                return new Promise<any>(async (resolve, reject) => {

                        await this.plt.ready();

                        let printerFound = false;
                        // no printer after 5 seconds => stop
                        const timeout = setTimeout(() => {
                                if (!printerFound) {
                                        this.ble.stopScan();
                                        reject({
                                                code: PRINT_ERROR_CODES.NO_PRINTER_FOUND,
                                                message: 'No printer found during bluetooth scan'
                                        });
                                }
                        }, SCAN_DELAY * 1000);
                        const print = (id: string) => {
                                printerFound = true;
                                clearTimeout(timeout);
                                this.ble.stopScan();
                                this.printZblToDevice(zblMessage, id).then(resolve)
                                        .catch(error => reject({code: PRINT_ERROR_CODES.PRINT_ERROR, message: error}));
                        };
                        this.ble.startScan([]).subscribe(device => {
                                        if (printerFound) {
                                                return;
                                        }
                                        const name: string = device.name;
                                        const ID = device.id;
                                        if (name && name.toUpperCase().startsWith(PRINTER_NAME_PREFIX)) {
                                                print(ID);
                                        } else {
                                                this.ble.connect(ID).subscribe(infos => {
                                                        if (!printerFound
                                                                && infos.services
                                                                && (infos.services as Array<string>).includes(ZEBRA_SERVICE)) {
                                                                print(ID);
                                                        }
                                                });
                                        }
                                }
                        );
                });
        }

        /**
         * Print zbl to a device by its id
         */
        printZblToDevice(zbl: string, deviceId: string) {
                return new Promise((resolve, reject) => {
                        /* tslint:disable:no-string-literal */
                        cordova.plugins['zbtprinter'].print(deviceId, zbl,
                                () => {
                                        console.log('imprimé');
                                        resolve();
                                }, (fail) => {
                                        reject(fail);
                                }
                        );
                });
        }
}
