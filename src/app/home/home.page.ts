import {Component} from '@angular/core';
import {ZebraPrinterService} from '../zebra-printer.service';
import {LoadingController} from '@ionic/angular';

@Component({
        selector: 'app-home',
        templateUrl: 'home.page.html',
        styleUrls: ['home.page.scss'],
})
export class HomePage {
        private message: string;
        private loading = false;

        constructor(private zebraPrinter: ZebraPrinterService) {
        }

        async print() {
                if (this.loading) {
                        return;
                }
                this.message = 'Impression en cours...';
                this.loading = true;

                const start = new Date().getTime();

                this.zebraPrinter.printZbl('^XA^FO20,20^A0N,25,25^FDThis is a ZPL test.^FS^XZ')
                        .then(() => {
                                const end = new Date().getTime();
                                this.message = 'Imprimé avec succès en ' + ((end - start) / 1000) + ' seconds';
                        })
                        .catch(error =>
                                this.message = 'Erreur : ' + error.message
                        )
                        .finally(() => this.loading = false);
        }
}
