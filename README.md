# Zebra Printer POC
by Nicolas Janel _(janel.nicolas@zenside.fr)_

This POC allows to print ZPL code to a Zebra printer using Bluetooth connection.

### Installation
`npm install`  
`ionic cordova run android`

### Use POC
To print label the Zebra printer should be turned on and the bluetooth of the device should be activated (the POC doesn't explicitly check bluetooth status before scan). 

`ZebraPrinter.print(zpl:string)` try to print some zpl code to a Zebra printer accesible by bluetooth. 

You can install the "Zebra Printer Setup" application for troubleshooting and to configure the printer (the fonction "Calibrate" allows to adapt printer to paper size). 

`HomeComponent.ts` presents an example of usage of service `ZebraPrinter`. This component is used when running the project.

### Use POC in another project

To use the connector in your project follow those steps :
 1. install the following dependencies    
    * `ionic cordova plugin add cordova-plugin-ble-central`
    * `ionic cordova plugin add cordova-plugin-zebra-printer`
    * `npm install @ionic-native/ble`
 2. import service `BLE (@ionic-native/ble/ngx)` in `providers` array of your module definition  
 3. copy the service ZebraPrinter to your project.  
